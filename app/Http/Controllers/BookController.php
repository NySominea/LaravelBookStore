<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category_id = '';
        $size =$request->input('size');
        if(!$size){
            $size = 10   ;
        }

        $sort = $request->input('sort');
        $order = $request->input('order');
        if($sort && $order){
            $books = Book::orderBy($sort,$order);
        }else{
            $books = Book::orderBy('created_at','desc');
        }

        $search = $request->input('search');
        $book_category = $request->input('book_category');
        $book_status = $request->input('book_status');

        $books->where(function($book) use ($search){
                $book->orWhere('title','LIKE','%'.$search.'%')
                ->orWhere('author','LIKE', '%'.$search.'%')
                ->orWhere('published_year','LIKE', '%'.$search.'%');
            });
              
        if($book_category!= null){
            if($book_category != 'all'){
                $category = Category::where('name',$book_category)->first();
                $category_id = $category->id;
                $books->where('category_id',$category->id);
            }   
        } 

        if($book_status != null){
            if($book_status != 'all'){
                $books->where('status',$book_status == "enable" ? 1 : 0);
             } 
        }    
            
        
        $books = $books->paginate($size);
        $categories = Category::orderBy('name','asc')->get();
        return view('admin.book.index')->with([
            'books' => $books,
            'book_category' => $category_id,
            'categories' => $categories,
            'size' => $size,
            'search' => $search,
            'book_status' => $book_status,
            'sort' => $sort,
            'order' => $order
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status',1)->orderBy('name','asc')->get();
        $category_dropdown = [];
        foreach($categories as $category){
             if(count($category->children) == 0)
             $category_dropdown[$category->id] = $category->name;
        }
        // return $category_dropdown;
        return view('admin.book.create')->with([
            'categories' => $category_dropdown
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'slug' => 'unique:books',
            'author' => 'required',
            'category' => 'required',
            'published' => 'required | numeric',
            'image' => 'image | mimes:jpg,jpeg,png',
            'file' => 'required|max:10000'
        ]);

        if($request->hasFile('image')){
            $image = $request->file('image');

            $imageNameWithExt = $image->getClientOriginalName();

            $imageName = pathInfo($imageNameWithExt,PATHINFO_FILENAME);

            $extension = $image->getClientOriginalExtension();

            $newImageName = $imageName.'_'.time().'.'.$extension;

            $pathImage = $image->storeAs('public/images',$newImageName);

            // Resize

            $thumbnailpath = public_path('storage/images/'.$newImageName);

            Image::make($thumbnailpath)->resize(250, 350)->save($thumbnailpath);

        }else{
            $newImageName = 'default.png';
        }

        if($request->hasFile('file')){
            $file = $request->file('file');

            $fileNameWithExt = $file->getClientOriginalName();

            $fileName = pathInfo($fileNameWithExt,PATHINFO_FILENAME);

            $extension = $file->getClientOriginalExtension();

            $newFileName = $fileName.'.'.$extension;

            $pathFile = $file->storeAs('public/files',$newFileName);
        }

        $book = new Book;
        $book->title = $request->input('title');
        $book->slug = $request->input('slug') ? $request->input('slug') : str_slug($request->input('title'), '-');
        $book->author = $request->input('author');
        $book->category_id = $request->input('category');
        $book->description = $request->input('description');
        $book->published_year = $request->input('published');
        $book->image = $newImageName;
        $book->file = $newFileName;
        $book->status = $request->input('status') == "on" ? 1 : 0;
        $book->user_id = auth()->user()->id;
        $book->view = 0;
        $book->download = 0;

        $book->save();

        if($request->input('save_continue')){
            return redirect()->route('admin.book.create')->with([
                'success' => $book->title . " book has been created."
            ]);
        }

        return redirect()->route('admin.book.index')->with([
            'success' => $book->title . " book has been created."
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::where('status',1)->orderBy('name','asc')->get();
        $category_dropdown = [];
        foreach($categories as $category){
            if(count($category->children) == 0)
             $category_dropdown[$category->id] = $category->name;
        }

        $book = Book::findOrFail($id);

        return view('admin.book.create')->with([
            'book' => $book,
            'categories' => $category_dropdown
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'author' => 'required',
            'published' => 'required | numeric',
            'image' => 'image | mimes:jpg,jpeg,png',
            'file' => 'max:10000'
        ]);

        $newImageName = '';
        $newFileName = '';
        if($request->hasFile('image')){
            $image = $request->file('image');

            $imageNameWithExt = $image->getClientOriginalName();

            $imageName = pathInfo($imageNameWithExt,PATHINFO_FILENAME);

            $extension = $image->getClientOriginalExtension();

            $newImageName = $imageName.'_'.time().'.'.$extension;

            $pathImage = $image->storeAs('public/images',$newImageName);

            // Resize

            $thumbnailpath = public_path('storage/images/'.$newImageName);

            Image::make($thumbnailpath)->resize(250, 350)->save($thumbnailpath);
        }

        if($request->hasFile('file')){
            $file = $request->file('file');

            $fileNameWithExt = $file->getClientOriginalName();

            $fileName = pathInfo($fileNameWithExt,PATHINFO_FILENAME);

            $extension = $file->getClientOriginalExtension();

            $newFileName = $fileName.'.'.$extension;

            $pathFile = $file->storeAs('public/files',$newFileName);
        }

        $book = Book::findOrFail($id);
        $book->title = $request->input('title');
        $book->slug = $request->input('slug') ? $request->input('slug') : str_slug($request->input('title'), '-');
        $book->author = $request->input('author');
        $book->category_id = $request->input('category');
        $book->description = $request->input('description');
        $book->published_year = $request->input('published');
        if($newImageName) $book->image = $newImageName;
        if($newFileName) $book->file = $newFileName;
        $book->status = $request->input('status') == "on" ? 1 : 0;

        $book->save();

        return redirect()->route('admin.book.index')->with([
            'success' => $book->title . " book has been successfully updated."
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $book = Book::findOrFail($id);
        $name = $book->title;
        $book->delete();

        return redirect()->route('admin.book.index')->with([
            'success' => $name . ' book has been successfuly deleted.'
        ]);
    }

    public function clone($book_id){
        $categories = Category::where('status',1)->orderBy('name','asc')->get();
        $category_dropdown = [];
        foreach($categories as $category){
            if(count($category->children) == 0)
             $category_dropdown[$category->id] = $category->name;
        }

        $book = Book::findOrFail($book_id);

        return view('admin.book.create')->with([
            'book' => $book,
            'categories' => $category_dropdown,
            'clone' => 'clone'
        ]);
    }
}
