<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Book;
use App\User;
use App\Category;
class AdminController extends Controller
{

    public function index(){
        $book_count = Book::count();
        $category_count = Category::count();
        $user_count = User::count();
        $data = [
            'book' => $book_count,
            'category' => $category_count,
            'user' => $user_count
        ];
        error_log('Some message here.');

        return view('admin.index')->with('total',$data);
    }

    public function login(){
        return view('admin.login');
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('admin.login');
    }
}
