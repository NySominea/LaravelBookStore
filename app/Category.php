<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Book;
class Category extends Model
{
    public function books(){
        return $this->hasMany(Book::class);
    }

    public function countBook(){
        return $this->books()->where('status',1)->count();
    }

    public function getAmountOfBooks($number){
        return $this->books()->take($number)->get();
    }

    public function parent() {
        return $this->belongsTo('App\Category', 'parent_id');
    }
    
    public function children() {
        return $this->hasMany('App\Category', 'parent_id'); //get all subs. NOT RECURSIVE
    }
}
