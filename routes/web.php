<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/admin/login',['uses' => 'AdminController@login', 'as' => 'admin.login']);
Route::post('/admin/logout',['uses' => 'AdminController@logout', 'as' => 'admin.logout']);

Route::group(['prefix' => '/admin', 'middleware' => 'auth' ], function () {
    Route::get('dashboard',['uses' => 'AdminController@index', 'as' => 'admin.dashboard']);
    Route::resource('book','BookController',['as' => 'admin']);
    Route::get('/book/{book_id}/clone','BookController@clone')->name('admin.book.clone');

    Route::resource('category','CategoryController',['as' => 'admin']);
    Route::get('/category/{category_id}/clone','CategoryController@clone')->name('admin.category.clone');

    Route::resource('user','UserController',['as' => 'admin']);
});


Route::get('/',['uses' => 'PageController@index', 'as' => 'home']);
Route::get('/about-us',['uses' => 'PageController@about', 'as' => 'about']);
Route::get('/contact-us',['uses' => 'PageController@contact', 'as' => 'contact']);
Route::get('/search',['uses' => 'PageController@search', 'as' => 'search']);
Route::get('/searchByCategory',['uses' => 'PageController@searchByCategory', 'as' => 'searchByCategory']);
Route::get('/book',['uses' => 'PageController@book', 'as' => 'book']);
Route::get('/book/category/{id}', ['uses' => 'PageController@bookByCategory', 'as' => 'category']);
Route::get('/book/{id}',['uses' => 'PageController@bookDetail', 'as' => 'detail']);
Route::get('/count-view/{id}',['uses' => 'PageController@countView', 'as' => 'view']);
Route::get('/count-download/{id}',['uses' => 'PageController@countDownload', 'as' => 'download']);



// Route::get('/home', 'HomeController@index')->name('home');

