$(document).ready(function(){


    $('#name').keyup(function(){
        var text = $(this).val();
        console.log(text);
        $('#slug-name').val(text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-'))
        ;
    });

    $('.collapse-parent').click(function(){
        var id = $(this).attr('href');
        var item = $(this);
        $(id).on('shown.bs.collapse', function () {
            item.find('i').removeClass('fa-plus').addClass('fa-minus');
         });
        $(id).on('hidden.bs.collapse', function () {
            item.find('i').removeClass('fa-minus').addClass('fa-plus');
         });
    });

    $('.sort').click(function(e){
        e.preventDefault();
        var icon = $(this).find('i');
        var url = window.location.href;
        var sort = $(this).attr('href');
        

        url = removeURLParameter(url,'sort');
        url = removeURLParameter(url,'order');

        if(checkExistUrlParameter(url)){
            url = url + "&";
        }else{
            if(url.indexOf('?') == -1){
                
                url = url + "?";
            }else{
                url = url;
            }
        }
        window.location.href = url + sort;
    });

    $('#category_size').change(function(){
        var size = ($(this).val());
        url = window.location.href;
        url = removeURLParameter(url,'size');
        
        if(checkExistUrlParameter(url)){
            url = url + "&" + size;
        }else{
            if(url.indexOf('?') == -1){
                
                url = url + "?" + size;
            }else{
                url = url + size;
            }
        }
        window.location.href=url;
    });

    $('.page').click(function(e){
        e.preventDefault()
        var size = $(this).attr('href');
        url = window.location.href;
        url = removeURLParameter(url,'size');
        if(checkExistUrlParameter(url)){
            url = url + "&" + size;
        }else{
            if(url.indexOf('?') == -1){
                
                url = url + "?" + size;
            }else{
                url = url + size;
            }
                
        }
        window.location.href=url;

    });

    function checkExistUrlParameter(url){
        var arr = url.split('?');
        
        if (arr.length > 1 && arr[1] !== '') {
            return true;
        }
        return false;
    }
    function removeURLParameter(url, parameter) {
        //prefer to use l.search if you have a location/link object
        var urlparts= url.split('?');   
        if (urlparts.length>=2) {
            
            var prefix= encodeURIComponent(parameter)+'=';
            var pars= urlparts[1].split(/[&;]/g);
    
            //reverse iteration as may be destructive
            for (var i= pars.length; i-- > 0;) {    
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                    pars.splice(i, 1);
                }
            }
    
            url= urlparts[0]+'?'+pars.join('&');
            return url;
        } else {
            return url;
        }
    }

    $('#save-continue').click(function(e){
        e.preventDefault();
        var form = $(this).parents('.card').find('form');
        form.append("<input type='hidden' name='save_continue' value='1'>");
        form.submit();
    });

    $('#file-upload').click(function(e){
        e.preventDefault();
        $('#file').click();
    });

    $('#file').change(function(){
       var fileName = $('#file').val().replace(/.*(\/|\\)/, '');
       $("#file-name").text(fileName);
    });

    $('#image-upload').click(function(){
        $('#image').click();
    });

    $('#image').change(function(){
        var _URL = window.URL || window.webkitURL;
        var file, image;
        if((file = this.files[0])){
            image = new Image();
            image.src = _URL.createObjectURL(file);
            image.onload = function(){

            };
            image.onerror = function(){
                alert("Not a valid Image: " + file.type);
                $('#image-upload').attr('src', window.location.origin + '/assets/images/250x350.png');
            };
        }
        readURL(this);
    });

    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image-upload').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
    }

    $('.btn-delete-book').click(function(e){
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var route = "/admin/book/";
        deleteConfirm(route, dataId);
    });

    $('.btn-delete-category').click(function(e){
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var route = "/admin/category/";
        deleteConfirm(route, dataId);
    });

    $('.btn-delete-user').click(function(e){
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var route = "/admin/user/";
        deleteConfirm(route, dataId);
    });

    function deleteConfirm(route,id){
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this record!",
            icon: "warning",
            buttons: [
              'No, cancel it!',
              'Yes, I am sure!'
            ],
            dangerMode: true,
            
          }).then(function(isConfirm) {
            if (isConfirm) {
              swal({
                title: 'Delelted!',
                icon: 'success',
                timer: 500,
              }).then(function() {
                  $('form').attr('action',route + id);
                  $('form').submit(); // <--- submit form programmatically
              });
            }
          });
    }
});