$(document).ready(function(){

    

    $('.collapse-parent').click(function(){
        var id = $(this).attr('href');
        var item = $(this);
        $(id).on('shown.bs.collapse', function () {
            item.find('i').removeClass('fa-plus').addClass('fa-minus');
         });
        $(id).on('hidden.bs.collapse', function () {
            item.find('i').removeClass('fa-minus').addClass('fa-plus');
         });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#view-book').click(function(){
        var id = $(this).attr('data-id'); 
        $.ajax(
            {
                url: "/count-view/"+id,
                type: 'GET', // replaced from put
                dataType: "JSON",
                data: {
                    "id": id // method and token not needed in data
                },
                success: function (response)
                {
                    console.log(response); // see the reponse sent
                },
                error: function(xhr) {
                    console.log(xhr.responseText); // this line will save you tons of hours while debugging
                }
            });
    });


    $("#btn-download").click(function(){
        var id = $(this).attr('data-id'); 
        $.ajax(
            {
                url: "/count-download/"+id,
                type: 'GET', // replaced from put
                dataType: "JSON",
                data: {
                    "id": id // method and token not needed in data
                },
                success: function (response)
                {
                    console.log(response); // see the reponse sent
                },
                error: function(xhr) {
                    console.log(xhr.responseText); // this line will save you tons of hours while debugging
                }
            });
    });
});