@extends('admin.fragments.master')

<!-- Breadcrumbs-->
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.user.index')}}">User</a></li>
    </ol>
@endsection

@section('content')
<div class="container-fluid pb-4">

    <div class="card">
        <div class="card-header">
            <strong>{{isset($user) && $user ? "Edit User " : "New User"}}  </strong>
           
        </div>
        
        <div class="card-body">      
            {!!Form::open(['route' => isset($user) && $user ? ["admin.user.update",$user->id] : 'admin.user.store', 'method' => 'POST', 'class' => 'form-inline', 'autocomplete' => 'off'])!!}       
            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0">
                  {!!Form::label('name','Username *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9"> 
                  {!!Form::text('name',isset($user) && $user ? $user->name : "",['class' => 'form-control w-100', 'placeholder' => 'Username'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0">
                  {!!Form::label('email','Email *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9"> 
                  {!!Form::email('email',isset($user) && $user ? $user->email : "",['class' => 'form-control w-100', 'placeholder' => 'Email'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0">
                  {!!Form::label('password','Password *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9"> 
                  {!!Form::password('password',['class' => 'form-control w-100', 'placeholder' => 'Password'])!!}
              </div>
            </div>
            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0">
                  {!!Form::label('confirm-password','Confirm Password *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9"> 
                  {!!Form::password('confirm-password',['class' => 'form-control w-100', 'placeholder' => 'Confirm Password'])!!}
              </div>
            </div>
        </div>
        <div class="card-footer">
            {{isset($user) && $user ? Form::hidden('_method','PUT') : ''}} 
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="{{ route('admin.user.index') }}" class="btn btn-danger text-white"><i class="fa fa-ban"></i> Cancel</a>
        </div>
        {!!Form::close()!!}
      </div>    
       
</div>
@endsection