@extends('admin.fragments.master')

@section('breadcrumb')
<!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.book.index')}}">Book</a></li>
        <li class="breadcrumb-item active">{{isset($book) && $book ? $book->title . " / Edit" : "New"}}</li>
    </ol>
@endsection

@section('content')
<div class="container-fluid pb-4">
    <div class="card mb-5">
        <div class="card-header">
          <strong>New Book</strong>  
        </div>
        <div class="card-body ">
          {!!Form::open(['route' => isset($book) && $book && !isset($clone) ? ['admin.book.update',$book->id] : 'admin.book.store', 'method' => 'POST', 'class' => 'form-inline', 'files' => true])!!}
            
            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('title','Book Title *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::text('title', isset($book) && $book ? $book->title : '',['id' => 'name','class' => 'form-control w-100', 'placeholder' => 'Book Title'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('slug','Slug Title *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::text('slug', isset($book) && $book ? $book->slug : '',['id' => 'slug-name', 'disabled' => 'disabled' ,'class' => 'form-control w-100', 'placeholder' => 'Slug Title'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('category','Book Category *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::select('category', $categories, isset($book) && $book ? $book->category->id : null ,['class' => 'form-control w-100'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('author','Book Author *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::text('author',isset($book) && $book ? $book->author : '',['class' => 'form-control w-100', 'placeholder' => 'Book Author'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('published','Book Published Year *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::text('published',isset($book) && $book ? $book->published_year : '',['class' => 'form-control w-100', 'placeholder' => 'Book Published Year'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('description','Book Description',['class' => 'd-flex align-items-start justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::textarea('description', isset($book) && $book ? $book->description : '', ['size' => '30x7','class' => 'form-control w-100', 'id' => 'summernote'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('image','Book Image',['class' => 'd-flex justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0 text-center"> 
                <img id="image-upload" src="{{asset(isset($book) && !isset($clone)? 'storage/images/'.$book->image : 'assets/images/250x350.png')}}">
                <input name="image" type="file" id="image" class="d-none">  
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('file','Book File *',['class' => 'd-flex justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                <input id="file" type="file" name="file" class="d-none" value={{ isset($book) && !isset($clone) ? '/storage/files/'.$book->file : "No file chosen"}}/>
                <button id="file-upload" class="btn btn-info btn-sm mr-2">Choose File</button><span id="file-name">{{isset($book) && !isset($clone) ? $book->file : "No file chosen"}}</span>
              </div>
            </div>
            
            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0 pr-0">
                  {!!Form::label('status','Book Status *',['class' => 'justify-content-start'])!!}
                </div>
                 <div class="col-md-9 pl-0 pr-0"> 
                    <label class="switch">
                        <input name="status" type="checkbox" {{isset($book) && $book ? $book->status ? "checked" : "" : "checked"}}>
                        <span class="slider"></span>
                        
                    </label>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <a href="{{ route('admin.book.index') }}" class="btn btn-danger text-white"><i class="fa fa-ban"></i> Cancel</a>
            {{isset($book) && $book && !isset($clone)? Form::hidden('_method','PUT') : ''}} 
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            @if(!isset($book) || isset($clone))
                <button id="save-continue" type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save & Continue</button>
            @endif
        </div>
        {!!Form::close()!!}
      </div>     
</div>
@endsection