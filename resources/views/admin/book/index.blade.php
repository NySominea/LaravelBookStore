@extends('admin.fragments.master')

@section('breadcrumb')
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Book</li>
</ol>
@endsection

@section('content')
<div class="container-fluid pb-4">
    <div class="card mb-5">
        <div class="card-header">
          <a href="{{route('admin.book.create')}}" class="btn btn-primary text-white"><i class="fa fa-plus"></i> New Book</a>
        </div>
        <div class="card-body">
            <div class="row mb-3">
                <div class="col-sm-12 col-md-4">
                  <div class="form-inline" >
                    <label>Show &nbsp; 
                      <select id="category_size" name="size_table" class="form-control form-control-sm">
                        <option {{$size == 10 ? 'selected' : ''}} value="size=10">10</option>
                        <option {{$size == 25 ? 'selected' : ''}} value="size=25">25</option>
                        <option {{$size == 50 ? 'selected' : ''}} value="size=50">50</option>
                        <option {{$size == 100 ? 'selected' : ''}} value="size=100">100</option>
                      </select> &nbsp; entries
                    </label>
                  </div>
                </div> 
                <div class="col-sm-12 col-md-8 text-right">
                  <form class="form-inline" method="GET" action="{{route('admin.book.index')}}">
                        <label class="sr-only" for="book_name">Book Title</label>
                        <input type="text" name="search" class="form-control mb-2 mr-sm-3" id="book-name" placeholder="Search for ..."
                                value="{{isset($search) && $search ? $search : ''}}">
                        
                        <label class="sr-only" for="status">Category</label>
                        <select id="book-status" name="book_category" class="form-control mb-2 mr-sm-3">
                            <option value="all">Category</option>
                            @foreach($categories as $category)
                              <option {{isset($book_category) && $book_category == $category->id ? 'selected' : ''  }} value="{{$category->name}}">{{$category->name}}</option>  
                            @endforeach
                        </select> 

                        <label class="sr-only" for="status">Status</label>
                        <select id="book-status" name="book_status" class="form-control mb-2 mr-sm-3">
                            <option value="all">Status</option>
                            <option {{isset($book_status) && $book_status == "enable" ? 'selected'  : ''}} value="enable">Enable</option>
                            <option {{isset($book_status) && $book_status == "disable" ? 'selected' : ''}} value="disable">Disable</option>
                        </select> 
                      
                        <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i></button>
                      </form>
                </div>
              </div>
          <div class="table-responsive">
            <table class="table table-bordered  table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Title <small><a href="sort=title&order={{isset($order)?$order=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('title')}}"></i></a></small></th>
                  <th>Category </th>
                  <th>Author <small><a href="sort=author&order={{isset($order)?$order=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('author')}}"></i></a></small></th>
                  <th>Year <small><a href="sort=published_year&order={{isset($order)?$order=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('published_year')}}"></i></a></small></th>
                  <th>Status <small><a href="sort=status&order={{isset($order)?$order=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('status')}}"></i></a></small></th>
                  <th>View</th>
                  <th>Down</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Title</th>
                  <th>Category</th>
                  <th>Author</th>
                  <th>Year</th>
                  <th>Status</th>
                  <th>View</th>
                  <th>Down</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                @if(isset($books) && count($books) > 0)
                    @foreach($books as $key => $book)
                        <tr>
                            <td class="text-center">{{$key + 1}}</td>
                            <td>{{$book->title}}</td>
                            <td>{{$book->category->name}}</td>
                            <td>{{$book->author}}</td>
                            <td>{{$book->published_year}}</td>
                            @if($book->status == 1) 
                              <td class="text-center"><span class='badge badge-success'>Enabled</span></td>
                            @else 
                              <td class="text-center"><span class='badge badge-danger'>Disabled</span></td>
                            @endif
                            <td class="text-center h5"><span class='badge badge-info'>{{$book->view}}</span></td>
                            <td class="text-center h5"><span class='badge badge-info'>{{$book->download}}</span></td>
                            <td>
                                <a href="{{route('admin.book.edit',$book->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                <a href="{{route('admin.book.clone',$book->id)}}" class="btn btn-success btn-sm"><i class="fa fa-clone"></i></a>
                                {!!Form::open(['route' => ['admin.book.destroy', $book->id], 'method' => 'POST', 'class' => 'd-inline'])!!}
                                   {{Form::hidden('_method','DELETE')}}
                                   <button type="submit" data-id={{$book->id}}  class="btn btn-danger btn-sm btn-delete-book" ><i class="fa fa-trash-o "></i></button>
                                 
                                {!!Form::close()!!}
                              </td>
                        </tr>
                    @endforeach
                @endif
              </tbody>
            </table>
          </div>
          <span>Showing  {{ $books->firstItem() }} - {{ $books->lastItem() }} of {{$books->total()}} entries,
            </span>
            Per page:
            <span class="text-dark">
              <a href="size=10" class="page text-dark {{$size == 10 ? 'font-weight-bold' : ''}}">10</a>, 
              <a href="size=25" class="page text-dark {{$size == 25 ? 'font-weight-bold' : ''}}">25</a>, 
              <a href="size=50" class="page text-dark {{$size == 50 ? 'font-weight-bold' : ''}}">50</a>,
              <a href="size=100" class="page text-dark {{$size == 100 ? 'font-weight-bold' : ''}}">100</a>
            </span>
            <div class="float-right">
  
                    @if ($books->hasPages())
                        {{ $books->appends(request()->input())->links() }}
                    @endif
            </div>
        </div>
      </div>     
</div>
@endsection