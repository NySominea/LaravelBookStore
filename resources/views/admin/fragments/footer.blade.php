<footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © {{config('app.name','BookStore')}} 2018</small>
        </div>
      </div>
</footer>