<!DOCTYPE html>
<html lang="en">
<head>
<title>
    {{config('app.name','BookStore')}}
</title>
@include('admin.fragments.head')
</head>
<body>
<div class="fixed-nav sticky-footer bg-dark" id="page-top">
    @include('admin.fragments.header')
    
    <div class="content-wrapper pt-4">
        <div class="container-fluid mt-5">
            @yield('breadcrumb')
        </div>

        <div class="container-fluid">
            @include('admin.fragments.message')
        </div>

        @yield('content')
    </div>

    @include('admin.fragments.footer')
</div>
</body>
@include('admin.fragments.scripts')


