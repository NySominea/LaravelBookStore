<script src="{{asset('admin/vendor/jquery/jquery.min.js')}}"></script>

<script src="{{asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Core plugin JavaScript-->
<script src="{{asset('admin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
<!-- Page level plugin JavaScript-->
{{-- <script src="{{asset('admin/vendor/chart.js/Chart.min.js')}}"></script> --}}
<script src="{{asset('admin/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
<!-- Custom scripts for all pages-->
<script src="{{asset('admin/js/sb-admin.min.js')}}"></script>
<!-- Custom scripts for this page-->
<script src="{{asset('admin/js/sb-admin-datatables.min.js')}}"></script>
{{-- <script src="{{asset('admin/js/sb-admin-charts.min.js')}}"></script> --}}

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="{{asset('admin/js/custom.js')}}"></script>

<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 200,
        });
    });
  </script>