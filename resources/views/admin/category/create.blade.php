@extends('admin.fragments.master')

<!-- Breadcrumbs-->
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.category.index')}}">Book Category</a></li>
        <li class="breadcrumb-item active">{{isset($category) && $category ? $category->name . " / Edit" : "New"}}</li>
    </ol>
@endsection

@section('content')
<div class="container-fluid pb-4">

    <div class="card">
        <div class="card-header">
            <strong>{{isset($category) && $category ? "Edit Category " : "New Book"}}  
           
        </div>
        
        <div class="card-body">      
            {!!Form::open(['route' => isset($category) && $category && !isset($clone) ? ["admin.category.update",$category->id] : 'admin.category.store', 'method' => 'POST', 'class' => 'form-inline'])!!}       
            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0">
                  {!!Form::label('name','Category Name *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9"> 
                  {!!Form::text('name',isset($category) && $category ? $category->name : "",['id' => 'name', 'class' => 'form-control w-100', 'placeholder' => 'Category Name'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0">
                  {!!Form::label('slug','Slug Name *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9"> 
                  {!!Form::text('slug',isset($category) && $category ? $category->slug : "",['id' => 'slug-name', 'disabled' => 'disabled', 'class' => 'form-control w-100', 'placeholder' => 'Slug Name'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0">
                  {!!Form::label('parent','Parent Category',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9"> 
                {!!Form::select('parent', $categories, isset($category) && $category->parent_id ? $category->parent_id : null ,['class' => 'form-control w-100','placeholder' => 'Pick Parent Category'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
                <div class="col-md-3 text-left pl-0">
                  {!!Form::label('status','Category Status',['class' => 'justify-content-start'])!!}
                </div>
                 <div class="col-md-9"> 
                    <label class="switch">
                        <input name="status" type="checkbox" {{isset($category) && $category ? $category->status ? "checked" : "" : "checked"}}>
                        <span class="slider"></span>
                        
                    </label>
                </div>
            </div>

        </div>
        <div class="card-footer">
            {{isset($category) && $category && !isset($clone) ? Form::hidden('_method','PUT') : ''}} 
            <a href="{{ route('admin.category.index') }}" class="btn btn-danger text-white"><i class="fa fa-ban"></i> Cancel</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
           
            @if(!isset($category) || isset($clone))
                <button id="save-continue" type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save & Continue</button>
            @endif
        </div>
        {!!Form::close()!!}
      </div>    
       
</div>
@endsection