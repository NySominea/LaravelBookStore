@extends('admin.fragments.master')

@section('breadcrumb')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Book Category</li>
    </ol>
@endsection

@section('content')
<div class="container-fluid pb-4">
    
    <div class="card mb-5">
        <div class="card-header">
          <a href="{{route('admin.category.create')}}" class="btn btn-primary text-white"><i class="fa fa-plus"></i> New Category</a>
          <button class="btn btn-success float-right" data-toggle="modal" data-target="#category-view">Tree View</button>
        </div>
        <div class="card-body">
          
            <div class="row mb-3">
              <div class="col-sm-12 col-md-4">
                <div class="form-inline" >
                  <label>Show &nbsp; 
                    <select id="category_size" name="size_table" class="form-control form-control-sm">
                      <option {{$size == 10 ? 'selected' : ''}} value="size=10">10</option>
                      <option {{$size == 25 ? 'selected' : ''}} value="size=25">25</option>
                      <option {{$size == 50 ? 'selected' : ''}} value="size=50">50</option>
                      <option {{$size == 100 ? 'selected' : ''}} value="size=100">100</option>
                    </select> &nbsp; entries
                  </label>
                </div>
              </div> 
              <div class="col-sm-12 col-md-8 text-right">
                <form class="form-inline" method="GET" action="{{route('admin.category.index')}}">
                      <label class="sr-only" for="Category">Category Name</label>
                      <input type="text" name="category_name" class="form-control mb-2 mr-sm-3" id="category-name" placeholder="Category name"
                              value="{{isset($category_name) && $category_name ? $category_name : ''}}">
                      
                      <label class="sr-only" for="Category">Category Status</label>
                      <select id="category-status" name="category_status" class="form-control mb-2 mr-sm-3">
                          <option value="all">Select Status</option>
                          <option {{isset($category_status) && $category_status == "enable" ? 'selected'  : ''}} value="enable">Enable</option>
                          <option {{isset($category_status) && $category_status == "disable" ? 'selected' : ''}} value="disable">Disable</option>
                      </select> 
                    
                      <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i></button>
                    </form>
              </div>
            </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name <small><a href="sort=name&order={{isset($order)?$order=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('name')}}"></i></a></small></th>
                  <th>Parent</th>
                  <th>Status <small><a href="sort=status&order={{isset($order)?$order=='asc'?'desc':'asc' :'asc'}}" class="text-dark sort"><i class="fa {{getSortIcon('status')}}"></i></a></small></th>
                  <th>Book Count</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Parent</th>
                  <th>Status</th>
                  <th>Book Count</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                @if(isset($categories) && count($categories) > 0)
                    @foreach($categories as $key => $category)
                        <tr>
                            <td class="text-center">{{$key + 1}}</td>
                            <td>{{$category->name}}</td>
                            <td>{{$category->parent ? $category->parent->name : ''}}</td>
                            @if($category->status == 1) 
                              <td class="text-center"><span class='badge badge-success'>Enabled</span></td>
                            @else 
                              <td class="text-center"><span class='badge badge-danger'>Disabled</span></td>
                            @endif
                            
                            <td class="text-center h5"><span class=" badge badge-info">{{count($category->books)}}</span></td>
                            <td>
                                <a href="{{route('admin.category.edit',$category->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                <a href="{{route('admin.category.clone',$category->id)}}" class="btn btn-success btn-sm"><i class="fa fa-clone"></i></a>
                                {!!Form::open(['route' => ['admin.category.destroy', $category->id], 'method' => 'POST', 'class' => 'd-inline'])!!}
                                   {{Form::hidden('_method','DELETE')}}
                                   <button type="submit" data-id={{$category->id}} class="btn btn-danger btn-sm btn-delete-category" id="delete"><i class="fa fa-trash-o "></i></button>
                                 
                                {!!Form::close()!!}
                            </td>
                        </tr>
                    @endforeach
                @endif
              </tbody>
            </table>
          </div>

          <span>Showing  {{ $categories->firstItem() }} - {{ $categories->lastItem() }} of {{$categories->total()}} entries,
          </span>
          Per page:
          <span class="text-dark">
            <a href="size=10" class="page text-dark {{$size == 10 ? 'font-weight-bold' : ''}}">10</a>, 
            <a href="size=25" class="page text-dark {{$size == 25 ? 'font-weight-bold' : ''}}">25</a>, 
            <a href="size=50" class="page text-dark {{$size == 50 ? 'font-weight-bold' : ''}}">50</a>,
            <a href="size=100" class="page text-dark {{$size == 100 ? 'font-weight-bold' : ''}}">100</a>
          </span>
          <div class="float-right">

                  @if ($categories->hasPages())
                      {{ $categories->appends(request()->input())->links() }}
                  @endif
          </div>
        </div>
      </div>     
</div>

<!-- Modal -->
<div class="modal fade" id="category-view" tabindex="-1" role="dialog" aria-labelledby="category-viewl" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Category Tree</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @if(isset($categories) && count($categories) > 0)
        <div class="modal-body">
            <ul class="list-group">
                @foreach($categories as $category)
                  @if(!$category->parent)
                    @if(count($category->children) > 0)
                        
                        <a  data-toggle="collapse" href="#collapseCategory{{$category->id}}" role="button" aria-expanded="false" aria-controls="collapseCategory" class="list-group-item list-group-item-action d-flex  justify-content-between align-items-center collapse-parent">
                            <strong>{{$category->name}}</strong>
                            <span class="badge text-primary badge-pill"><i class="fa fa-minus"></i></span>
                        </a>
                        <div class="collapse show" id="collapseCategory{{$category->id}}">
                            <ul class="list-group">
                        @foreach($category->children as $child)
                            <a  style="padding-left:35px;" class="list-group-item list-group-item-action d-flex  justify-content-between align-items-center">
                                {{$child->name}}
                            </a>
                        @endforeach
                            </ul>
                        </div>
                    @else
                        <a href="{{route('category',$category->id)}}" class="list-group-item list-group-item-action d-flex  justify-content-between align-items-center">
                            {{$category->name}}
                        </a>
                    @endif
                  @endif
                  
                @endforeach
            </ul>
        </div>
        @endif
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

@endsection