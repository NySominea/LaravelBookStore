@extends('fragments.master')

@section('title')
    About Us
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb " class="bg-light">
<ol class="breadcrumb rounded-0 container bg-light">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">About Us</li>
</ol>
</nav>
@endsection

@section('content') 
    <div class="row">
        <div class="col-lg-10 offset-lg-1 col-md-12">
            <div class="card bg-light mb-3">
            <div class="card-header bg-primary text-white">About Us</div>
            <div class="card-body">
                <p class="card-text text-primary">Welcome to BookStore, a happy place for people who love books!!</p>
                <p class="card-text">BookStore is the place for book lovers of all ages, where reading, 
                    meeting and discussing books is a way of life.  We host many interesting author events, 
                    book clubs, and story times.  BookStore has an amazing selection of books and a 
                    knowledgeable staff to help you select the perfect book. We offer complimentary wrapping 
                    and can arrange to ship books out.  </p>
            </div>
            </div>
        </div>
    </div>
@endsection