@extends('fragments.master')

@section('title')
    About Us
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb " class="bg-light">
<ol class="breadcrumb rounded-0 container bg-light">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
</ol>
</nav>
@endsection

@section('content') 
    <div class="row">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1954.9315561373512!2d104.94030665791837!3d11.489797997959993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTHCsDI5JzIzLjMiTiAxMDTCsDU2JzI5LjAiRQ!5e0!3m2!1sen!2skh!4v1529207090712" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            <div class="container-fluid">
                <h4 class="title text-center mt-4"><b>Get In Touch With Us</b></h4>
                <p class="text-center title">________</p>
                <div class="row">
                    <div class="col-lg-5 offset-lg-1">
                        <h5 class="pb-3"><b>Our Location</b></h5>
                        <p>
                                #1. Doeum Kor village, Doeum Mean commune, Takhmao Town,  Kandal Province, Cambodia.
                        </p>
                        <p>
                                #2. Doeum Kor village, Doeum Mean commune, Takhmao Town,  Kandal Province, Cambodia.
                        </p>
                    </div>   
                    <div class="col-lg-5 offset-lg-1">
                        <h5 class="pb-3"><b>Contact</b></h5>
                        Tel: (+855) 93 355 045 <br>
                        Email: sominea.ny@gmail.com <br>
                    </div>   
                </div>
            </div>
    </div>
@endsection