@extends('fragments.master')

@section('title')
    Home
@endsection

@section('breadcrumb')
    <nav aria-label="breadcrumb " class="bg-light">
    <ol class="breadcrumb rounded-0 container bg-light">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
    </ol>
    </nav>
@endsection

@section('content') 

    @if(isset($categories))
    @foreach($categories as $sub)
        @foreach($sub->children as $category)
        <div class="row">
                @if($category->countBook() > 0)
                    <h4 class="pl-3 pb-2 text-weight-bold">{{$category->name}}</h4>
                    <div class="col-lg-12">
                        <div class="row">
                        @foreach($category->getAmountOfBooks(3) as $book)
                        
                            <div class="col-md-4 mb-4 col-sm-6 col-6">
                                <a href="{{route('detail',$book->slug)}}" data-id={{$book->id}} style="text-decoration:none;" class="text-left p-0">
                                    <div class="card ">
                                        <img class="card-img-top mw-100" src="{{asset('storage/images/'.$book->image)}}" alt="Book Image">
                                        <div class="card-body p-2">
                                            <h5 class="card-title text-dark">{{$book->title}} ({{$book->published_year}})</h5>
                                            <hr class="border border-secondary border-bottom-0  mt-1 mb-1"></hr>
                                            <p class="m-0 text-dark">{{$book->author}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>        
                        
                        @endforeach
                        </div> 
                    </div>
                @endif
            </div>  
        @endforeach
    @endforeach
    @else
        No Book Found
    @endif
@endsection