@extends('fragments.master')

@section('title')
    Home
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb " class="bg-light">
    <ol class="breadcrumb rounded-0 container bg-light">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('book')}}">Book</a></li>
        <li class="breadcrumb-item"><a href="{{route('category',$book->category->id)}}">{{$book->category->name}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$book->title}}</li>

    </ol>
</nav>
@endsection

@section('content') 
    @if(isset($book))
        <div class="row">
            <div class="col-sm-5 text-center mb-4">
                <img src="{{asset('storage/images/'.$book->image)}}" class="mw-100 w-95">
            </div>
            <div class="col-sm-7">
                <h3 class="font-weight-bold text-center mb-4">{{$book->title}}</h3>
                <table class="table"> 
                    <tbody>
                        <tr>
                            <td class="w-25 border-top border-dark"><strong>Category</strong></td> <td class="border-top border-dark">{{$book->category->name}}</td>
                        </tr>
                        <tr>
                            <td class="w-25 border-top border-dark"><strong>Author</strong></td> <td class="border-top border-dark">{{$book->author}}</td>
                        </tr
                        <tr>
                            <td class="w-25 border-top border-dark"><strong>Published</strong></td> <td class="border-top border-dark">{{$book->published_year}}</td>
                        </tr>
                        <tr>
                            <td class="w-25 border-top border-dark"><strong>Description</strong></td> <td class="border-top border-dark text-justify">{!!$book->description!!}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="text-right">
                    <a id="btn-download" data-id = {{$book->id}} href="{{asset('storage/files/'.$book->file)}}" class="btn btn-primary text-white">Download</a>
                </div>
            </div>
        </div>
    @endif
@endsection