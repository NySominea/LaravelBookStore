@extends('fragments.master')

@section('title')
    Home
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb " class="bg-light">
    <ol class="breadcrumb rounded-0 container bg-light">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
        @if(!isset($category))
        <li class="breadcrumb-item active" aria-current="page">Book</li>
        @else
        <li class="breadcrumb-item active" aria-current="page"><a href="{{route('book')}}">Book</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$category->name}}</li>
        @endif
    </ol>
</nav>
@endsection

@section('content') 

    @if(isset($books) && count($books) > 0)
    <div class="row">
    @foreach($books as $book)
        <div class="col-md-4 mb-4 col-sm-6 col-6">
            <a href="{{route('detail',$book->slug )}}" id="view-book" data-id={{$book->id}} style="text-decoration:none;" class="text-left p-0">
                <div class="card ">
                    <img class="card-img-top mw-100" src="{{asset('storage/images/'.$book->image)}}" alt="Book Image">
                    <div class="card-body p-2">
                        <h5 class="card-title text-dark">{{$book->title}} ({{$book->published_year}})</h5>
                        <hr class="border border-secondary border-bottom-0  mt-1 mb-1"></hr>
                        <p class="m-0 text-dark">{{$book->author}}</p>
                    </div>
                </div>
            </a>     
        </div>  
    @endforeach
    </div>
    @else
        No Book Found
    @endif
    @if ($books->hasPages())
        {{ $books->appends(request()->input())->links() }}
    @endif
@endsection