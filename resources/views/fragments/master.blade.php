<!DOCTYPE html>
<html lang="en">
<head>
    
<title>      
   {{config('app.name','BookStore')}} | @yield('title')

</title>
<link rel="icon" type="image/jpg" href="{{ asset('assets/images/site.jpg') }}">
@include('fragments.head')
</head>
<body>
<div class="app">
    @include('fragments.header')
    @yield('breadcrumb')
    
    <div class="container">
        <div class="row mt-4">
            <div class="col-lg-9 mb-4 order-lg-12">
                @yield('content')
            </div>
            <div class="col-lg-3 mb-4 order-lg-1 p-0">
                
                @if(isset($categories) && count($categories) > 0)
                    <div class="col-lg-12 mb-4">
                        <div class="list-group"> 
                            <p class="h5 list-group-item list-group-item-action bg-light font-weight-bold">Book Category</p> 
                        @foreach($categories as $category)
                            @if(count($category->children) > 0)
                                
                                <a  data-toggle="collapse" href="#collapseCategory{{$category->id}}" role="button" aria-expanded="false" aria-controls="collapseCategory" class="list-group-item list-group-item-action d-flex  justify-content-between align-items-center collapse-parent">
                                    {{$category->name}}
                                    <span class="badge text-primary badge-pill"><i class="fa fa-minus"></i></span>
                                </a>
                                <div class="collapse show" id="collapseCategory{{$category->id}}">
                                    <ul class="list-group">
                                @foreach($category->children as $child)
                                    <a  href="{{route('category',$child->slug)}}" style="padding-left:35px;" class="list-group-item list-group-item-action d-flex  justify-content-between align-items-center">
                                        {{$child->name}}
                                        <span class="badge badge-primary badge-pill">{{$child->countBook()}}</span>
                                    </a>
                                @endforeach
                                    </ul>
                                </div>
                            @else
                                <a href="{{route('category',$category->slug)}}" class="list-group-item list-group-item-action d-flex  justify-content-between align-items-center">
                                    {{$category->name}}
                                    <span class="badge badge-primary badge-pill">{{$category->countBook()}}</span>
                                </a>
                            @endif
                            
                            
                        @endforeach
                        </div> 
                    </div>
                @endif
                <div class="col-lg-12">
                    <div class="card">
                    <div class="card-header p-3">
                        <b class="h5 font-weight-bold">Search</b>
                    </div>
                    {!!Form::open(['route' => 'search', 'method' => 'get'])!!}
                    <div class="card-body p-3">
                        
                        <div class="form-group">
                            {!!Form::label('title', 'Book Title');!!}
                            {!!Form::text('title','',['class' => 'form-control']);!!}
                        </div>
                        <div class="form-group">
                            {!!Form::label('author', 'Book Author');!!}
                            {!!Form::text('author','',['class' => 'form-control']);!!}
                        </div>
                        <div class="form-group">
                            {!!Form::label('Year', 'Published Year');!!}
                            <div class="row">
                                <div class="col-md-6">
                                    {!!Form::label('from', 'From');!!}
                                    {!!Form::text('from','',['class' => 'form-control ']);!!}
                                </div>
                                <div class="col-md-6">
                                    {!!Form::label('to', 'To');!!}
                                    {!!Form::text('to','',['class' => 'form-control']);!!}
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="card-footer">
                        {!!Form::submit('Search',['class' => 'btn btn-primary w-100'])!!}
                    </div>
                    {!!Form::close()!!}
                </div>
                </div>
            </div>
            
        </div>
        
    </div>

    @include('fragments.footer')
</div>
</body>
@include('fragments.scripts')


