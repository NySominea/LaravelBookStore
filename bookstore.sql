-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 25, 2018 at 11:37 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `published_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `view` int(11) DEFAULT NULL,
  `download` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `slug`, `author`, `description`, `published_year`, `image`, `file`, `status`, `view`, `download`, `category_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'ASP.NET Core', 'aspnet-core', 'Andrew Lock', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">ASP.NET Core is a re-imagining of the .NET Framework that frees developers from Visual Studio and Windows. ASP.NET Core in Action is for C# developers without any web development experience who want to get started and productive using ASP.NET Core to build web applications.</span><br></p>', '2000', 'default.png', 'book.pdf', 1, 5, 0, 18, 1, '2018-06-24 20:12:42', '2018-06-25 02:16:25'),
(5, 'Java EE Web', 'java-ee-web', 'Brad Lees,', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">ASP.NET Core is a re-imagining of the .NET Framework that frees developers from Visual Studio and Windows. ASP.NET Core in Action is for C# developers without any web development experience who want to get started and productive using ASP.NET Core to build web applications.</span><br></p>', '1998', 'default.png', 'book.pdf', 1, 1, 0, 16, 1, '2018-06-24 20:17:11', '2018-06-25 02:15:10'),
(9, 'Beginning Android Games', 'beginning-android-games', 'J. F. DiMarzio', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">Learn all of the basics needed to join the ranks of successful Android game developers. You’ll start with game design fundamentals and Android programming basics, and then progress toward creating your own basic game engine and playable game apps that work on Android smartphones and tablets. Beginning Android…</span><br></p>', '2005', 'default.png', 'book.pdf', 1, 0, 0, 17, 1, '2018-06-24 20:21:23', '2018-06-25 02:13:57'),
(12, 'Hacking Marketing', 'hacking-marketing', 'J. F. DiMarzio', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">Learn all of the basics needed to join the ranks of successful Android game developers. You’ll start with game design fundamentals and Android programming basics, and then progress toward creating your own basic game engine and playable game apps that work on Android smartphones and tablets. Beginning Android…</span><br></p>', '2008', 'default.png', 'book.pdf', 1, 0, 0, 16, 1, '2018-06-24 20:22:38', '2018-06-25 02:13:35'),
(14, 'Search Engine Optimization', 'search-engine-optimization', 'J. F. DiMarzio', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">Learn all of the basics needed to join the ranks of successful Android game developers. You’ll start with game design fundamentals and Android programming basics, and then progress toward creating your own basic game engine and playable game apps that work on Android smartphones and tablets. Beginning Android…</span><br></p>', '2008', 'default.png', 'book.pdf', 1, 0, 0, 18, 1, '2018-06-24 20:23:22', '2018-06-25 02:13:27'),
(15, 'Mac OS X Leopard', 'mac-os-x-leopard', 'Bob LeVitus', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">Learn all of the basics needed to join the ranks of successful Android game developers. You’ll start with game design fundamentals and Android programming basics, and then progress toward creating your own basic game engine and playable game apps that work on Android smartphones and tablets. Beginning Android…</span><br></p>', '2003', 'default.png', 'book.pdf', 1, 1, 0, 18, 1, '2018-06-24 20:26:54', '2018-06-25 01:21:52'),
(16, 'SUSE Linux 9.3', 'suse-linux-93', 'Bob LeVitus', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">Learn all of the basics needed to join the ranks of successful Android game developers. You’ll start with game design fundamentals and Android programming basics, and then progress toward creating your own basic game engine and playable game apps that work on Android smartphones and tablets. Beginning Android…</span><br></p>', '2003', 'Screenshot from 2018-06-13 16-02-44_1529898242.png', 'book.pdf', 1, 4, 0, 18, 1, '2018-06-24 20:27:08', '2018-06-25 02:16:40'),
(24, 'Pro HTML5 Games', 'pro-html5-games', 'J. F. DiMarzio', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">Learn all of the basics needed to join the ranks of successful Android game developers. You’ll start with game design fundamentals and Android programming basics, and then progress toward creating your own basic game engine and playable game apps that work on Android smartphones and tablets. Beginning Android…</span><br></p>', '2005', 'default.png', 'book.pdf', 1, 0, 0, 17, 1, '2018-06-24 20:22:00', '2018-06-25 02:13:39'),
(26, 'CSS Refactoring', 'css-refactoring', 'Andrew Lock', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">ASP.NET Core is a re-imagining of the .NET Framework that frees developers from Visual Studio and Windows. ASP.NET Core in Action is for C# developers without any web development experience who want to get started and productive using ASP.NET Core to build web applications.</span><br></p>', '2000', 'default.png', 'book.pdf', 1, 0, 0, 16, 1, '2018-06-24 20:13:20', '2018-06-25 02:16:09'),
(27, 'The Python 3', 'the-python-3', 'Andrew Lock', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">ASP.NET Core is a re-imagining of the .NET Framework that frees developers from Visual Studio and Windows. ASP.NET Core in Action is for C# developers without any web development experience who want to get started and productive using ASP.NET Core to build web applications.</span><br></p>', '2000', 'default.png', 'book.pdf', 1, 0, 0, 16, 1, '2018-06-24 20:15:06', '2018-06-25 02:14:14'),
(28, 'Design Patterns in C#', 'design-patterns-in-c', 'Andrew Lock', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">ASP.NET Core is a re-imagining of the .NET Framework that frees developers from Visual Studio and Windows. ASP.NET Core in Action is for C# developers without any web development experience who want to get started and productive using ASP.NET Core to build web applications.</span><br></p>', '1999', 'default.png', 'book.pdf', 1, 0, 0, 18, 1, '2018-06-24 20:15:44', '2018-06-25 02:16:14'),
(29, 'Swift 4', 'swift-4', 'Brad Lees', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">ASP.NET Core is a re-imagining of the .NET Framework that frees developers from Visual Studio and Windows. ASP.NET Core in Action is for C# developers without any web development experience who want to get started and productive using ASP.NET Core to build web applications.</span><br></p>', '2001', 'default.png', 'book.pdf', 1, 0, 0, 17, 1, '2018-06-24 20:17:31', '2018-06-25 02:13:46'),
(30, 'MySQL 8 Document', 'mysql-8-document', 'Charles Bell', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">ASP.NET Core is a re-imagining of the .NET Framework that frees developers from Visual Studio and Windows. ASP.NET Core in Action is for C# developers without any web development experience who want to get started and productive using ASP.NET Core to build web applications.</span><br></p>', '2002', 'default.png', 'book.pdf', 1, 0, 0, 16, 1, '2018-06-24 20:19:41', '2018-06-25 02:13:49'),
(31, 'SQL Primer', 'sql-primer', 'Charles Bell', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">ASP.NET Core is a re-imagining of the .NET Framework that frees developers from Visual Studio and Windows. ASP.NET Core in Action is for C# developers without any web development experience who want to get started and productive using ASP.NET Core to build web applications.</span><br></p>', '2002', 'default.png', 'book.pdf', 1, 0, 0, 16, 1, '2018-06-24 20:20:26', '2018-06-25 02:13:43'),
(34, 'eBay Commerce', 'ebay-commerce1', 'J. F. DiMarzio', '<p><span style=\"color: rgb(78, 88, 96); font-family: \"Open Sans\", Arial, Verdana, sans-serif; font-size: 14px;\">Learn all of the basics needed to join the ranks of successful Android game developers. You’ll start with game design fundamentals and Android programming basics, and then progress toward creating your own basic game engine and playable game apps that work on Android smartphones and tablets. Beginning Android…</span><br></p>', '2008', 'default.png', 'book.pdf', 1, 0, 0, 17, 1, '2018-06-24 20:22:57', '2018-06-25 01:13:16');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `status`, `parent_id`, `user_id`, `created_at`, `updated_at`) VALUES
(14, 'IT', 'it', 1, NULL, 1, '2018-06-25 01:33:54', '2018-06-25 01:33:54'),
(15, 'Computer Science', 'computer-science', 1, NULL, 1, '2018-06-25 01:33:59', '2018-06-25 01:33:59'),
(16, 'Web Development', 'web-development', 1, 14, 1, '2018-06-25 01:34:13', '2018-06-25 01:34:13'),
(17, 'Database', 'database', 1, 14, 1, '2018-06-25 01:55:28', '2018-06-25 01:55:28'),
(18, 'Operating Systems', 'operating-systems', 1, 15, 1, '2018-06-25 01:55:36', '2018-06-25 01:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2018_06_16_153217_create_books_table', 1),
(8, '2018_06_16_153249_create_categories_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@bookstore.com', '$2y$10$waQwpSiklsRynclhesdjBugTwIz5MMGfW.X7SDU1ShQFYV6wQ3CPa', '7DN1KKvCLaAkPztmwnB3VwYQGUUhwDx1ETzNhFYQbiGDBCuDzmtYPT6OTmRE', '2018-06-24 20:10:10', '2018-06-24 20:10:10'),
(2, 'test', 'test@gmail.com', '$2y$10$NN0gDKz.JKCSpgTmMo6.G.6EkTgi6luyLvuHGw8Jp3L8Jt2E0QLoS', 'JPW7Ui5AIcT9Wnxw6R9CWh8Mow9UvlmJVxSz6fYZ8Sox8RY9ROMPDgdf3QoR', '2018-06-24 21:17:28', '2018-06-24 21:17:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_name_unique` (`name`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
